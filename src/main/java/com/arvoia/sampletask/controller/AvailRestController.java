package com.arvoia.sampletask.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.arvoia.sampletask.model.AvailResponse;
import com.arvoia.sampletask.service.ArvoiaService;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

@Configuration
@RestController
public class AvailRestController {

	@Autowired
	private RestTemplate restTemplate;
	@Autowired
	private ArvoiaService arvoiaService;

	@Value("${vendor.url}")
	private String vendorUrl;

	@GetMapping(value = "/avail", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody AvailResponse getAvailabilities() throws JsonParseException, JsonMappingException, IOException {
		String searchResult = getSearchResults();
		return arvoiaService.mapAvailResponseToArvoiaResponse(searchResult);
	}

	private String getSearchResults() {
		return restTemplate.getForEntity(vendorUrl, String.class).getBody();
	}
}
