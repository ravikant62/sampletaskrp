package com.arvoia.sampletask.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.arvoia.sampletask.exception.ApiError;
import com.arvoia.sampletask.exception.Error;

@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler{
	
	@ExceptionHandler(ResourceAccessException.class)
	protected ResponseEntity<Object> handleResourceAccessException(ResourceAccessException ex){
		Error error = new Error();
		error.setCode(1);
		error.setDescription("vendor service not available");
		ApiError apiError = new ApiError(error);
		return new ResponseEntity<Object>(apiError, HttpStatus.SERVICE_UNAVAILABLE);
	}
}
