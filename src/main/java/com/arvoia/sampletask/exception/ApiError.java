package com.arvoia.sampletask.exception;

public class ApiError {
	
	private String result;
	private Error error;
	
	public ApiError(Error error) {
		this.error = error;
		this.result = "error";
	}
	public String getResult() {
		return result;
	}

	public Error getError() {
		return error;
	}
}
