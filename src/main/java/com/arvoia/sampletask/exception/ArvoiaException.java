package com.arvoia.sampletask.exception;

public class ArvoiaException extends RuntimeException{
	
	private static final long serialVersionUID = 1L;

	public ArvoiaException() {
		super();
	}
	
	public ArvoiaException(String message) {
		super(message);
	}
	
	public ArvoiaException(String message, Throwable throwable) {
		super(message, throwable);
	}

}
