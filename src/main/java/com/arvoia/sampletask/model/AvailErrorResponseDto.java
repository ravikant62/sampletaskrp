package com.arvoia.sampletask.model;

import com.arvoia.sampletask.exception.Error;

public class AvailErrorResponseDto extends AvailResponse{

	private String result;
	private Error error;
	
	public AvailErrorResponseDto(Error error) {
		this.result = "error";
		this.error = error;
	}
	
	public String getResult() {
		return result;
	}

	public Error getError() {
		return error;
	}
}
