package com.arvoia.sampletask.model;

public class AvailSuccessResponseDto extends AvailResponse{

	private String result;
	private Offer offer;
	
	public AvailSuccessResponseDto(Offer offer) {
		this.offer = offer;
		this.result= "success";
	}
	
	public String getResult() {
		return result;
	}

	public Offer getOffer() {
		return offer;
	}
}
