package com.arvoia.sampletask.service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.arvoia.sampletask.exception.ArvoiaException;
import com.arvoia.sampletask.exception.Error;
import com.arvoia.sampletask.model.AvailErrorResponseDto;
import com.arvoia.sampletask.model.AvailResponse;
import com.arvoia.sampletask.model.AvailSuccessResponseDto;
import com.arvoia.sampletask.model.Offer;
import com.arvoia.sampletask.model.Vehicle;
import com.arvoia.sampletask.model.VendorResponse;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class ArvoiaService {
	
	public AvailResponse mapAvailResponseToArvoiaResponse(String response) {
	    try {
		    ObjectMapper objectMapper = new ObjectMapper();
		    objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		    VendorResponse vendorResponse = objectMapper.readValue(response, VendorResponse.class);
		    if(Objects.isNull(vendorResponse.getError())) {
		    	return mapSuccessDto(vendorResponse);
		    }else {
		    	return mapErrorDto(vendorResponse);
		    }
	    }catch(Exception ex) {
	    	throw new ArvoiaException("Exception while mapping response to dto",ex);
	    }
	}

	private AvailResponse mapErrorDto(VendorResponse vendorResponse) {
		Error error = new Error();
		error.setCode(vendorResponse.getError().getCode());
		error.setDescription(vendorResponse.getError().getDescription());
		return new AvailErrorResponseDto(error);
	}

	private AvailResponse mapSuccessDto(VendorResponse vendorResponse) {
		Offer offer = new Offer();
		offer.setAvailable(CollectionUtils.isEmpty(vendorResponse.getVehicles()) ? 0 : vendorResponse.getVehicles().size());
		List<Vehicle> sortedVehicle = vendorResponse.getVehicles().stream()
				  .sorted(Comparator.comparing(Vehicle::getBasePrice))
				  .collect(Collectors.toList());
		offer.setVehicles(sortedVehicle);
		offer.setTimestamp((DateTimeFormatter.ISO_LOCAL_DATE_TIME) 
                .format(LocalDateTime.now()));
		return new AvailSuccessResponseDto(offer);
	}
}
