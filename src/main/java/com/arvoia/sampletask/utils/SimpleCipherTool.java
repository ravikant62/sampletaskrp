package com.arvoia.sampletask.utils;

import java.util.HashMap;
import java.util.Map;

public class SimpleCipherTool {
	
	private String key;
	
	private static final Map<Character, Integer> characterMap = new HashMap<>();
	
	static {
		int start = 97;
		int count = 1;
		while(count<27) {
			characterMap.put((char)start, count);
			start++;
			count++;
		}
	}
	
	public SimpleCipherTool(String key) {
		this.key = key;
	}
	
	public String cipher(String text) {
		if(key==null || key.trim().length() == 0) {
			return "Please provide key";
		}
		if(text == null || text.trim().length() == 0) {
			return "Please provide PlainText for encryption";
		}
		StringBuilder strb = new StringBuilder();
		for(int i = 0 ; i< text.length(); i ++) {
			char keyAtIndex = key.toLowerCase().charAt(i % key.length());
			char characterAtIndex = text.charAt(i);
			if(characterAtIndex >= 'a' && characterAtIndex <= 'z') {
				characterAtIndex = (char)(characterAtIndex + characterMap.get(keyAtIndex));
				if(characterAtIndex > 'z') {
					characterAtIndex = (char)(characterAtIndex +'a'-'z'-1);
				}
			}
			strb.append(characterAtIndex);
		}
		return strb.toString();
	}
	
	public String decipher(String cipheredText) {
		if(key==null || key.trim().length() == 0) {
			return "Please provide key";
		}
		if(cipheredText == null || cipheredText.trim().length() == 0) {
			return "Please provide PlainText for encryption";
		}
		StringBuilder strb = new StringBuilder();
		for(int i =0; i< cipheredText.length(); i++) {
			char keyAtIndex = key.toLowerCase().charAt(i % key.length());
			char characterAtIndex = cipheredText.charAt(i);
			if(characterAtIndex >= 'a' && characterAtIndex <= 'z') {
				characterAtIndex = (char)(characterAtIndex - characterMap.get(keyAtIndex));
				if(characterAtIndex < 'a') {
					characterAtIndex = (char) (characterAtIndex-'a'+'z'+1);
				}
			}
			strb.append(characterAtIndex);
		}	
		return strb.toString();
	}
}
