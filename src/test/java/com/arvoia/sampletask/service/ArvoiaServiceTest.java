package com.arvoia.sampletask.service;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import com.arvoia.sampletask.exception.ArvoiaException;
import com.arvoia.sampletask.model.AvailErrorResponseDto;
import com.arvoia.sampletask.model.AvailResponse;
import com.arvoia.sampletask.model.AvailSuccessResponseDto;

@RunWith(MockitoJUnitRunner.class)
public class ArvoiaServiceTest {

	@InjectMocks
	private ArvoiaService arvoiaService;

	@Test
	public void givenSuccessResponse_whenMapAvailResponseToArvoiaResponse_thenShouldReturnAvailResponse() {
		AvailResponse response = arvoiaService.mapAvailResponseToArvoiaResponse(successVendorResponse());
		AvailSuccessResponseDto successResponse = (AvailSuccessResponseDto)response;
		assertTrue(successResponse.getResult().equals("success"));
		assertTrue(successResponse.getOffer().getAvailable() == 3);
		assertTrue(successResponse.getOffer().getVehicles().size() == 3);
	}

	@Test
	public void givenErrorResponse_whenMapAvailResponseToArvoiaResponse_thenShouldReturnAvailResponse() {
		AvailResponse response = arvoiaService.mapAvailResponseToArvoiaResponse(errorVendorResponse());
		AvailErrorResponseDto successResponse = (AvailErrorResponseDto)response;
		assertTrue(successResponse.getResult().equals("error"));
		assertTrue(successResponse.getError().getCode() == 600);
		assertTrue(successResponse.getError().getDescription().equals("system maintenance"));
	}
	
	@Test(expected = ArvoiaException.class)
	public void givenInvalidJsonResponse_whenMapAvailResponseToArvoiaResponse_thenShouldThrowException() {
		arvoiaService.mapAvailResponseToArvoiaResponse("invalid");
	}

	private String successVendorResponse(){
		return "{\"error\":null,\"vehicles\":[{\"brand\":\"Volvo\",\"category\":\"Luxury\",\"basePrice\":520.0,\"color\":\"grey\",\"doors\":4},{\"brand\":\"Volvo\",\"category\":\"Economy\",\"basePrice\":560.0,\"color\":\"white\",\"doors\":4},{\"brand\":\"Volvo\",\"category\":\"Economy\",\"basePrice\":500.0,\"color\":\"red\",\"doors\":3}]}";
	}

	private String errorVendorResponse(){
		return "{\"error\": {\"code\": 600, \"description\": \"system maintenance\"}, \"vehicles\": null}";
	} 

}
