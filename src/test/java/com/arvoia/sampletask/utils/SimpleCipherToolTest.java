package com.arvoia.sampletask.utils;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

public class SimpleCipherToolTest {

  private SimpleCipherTool underTest;

  @Test
  public void testCipher() {
    // given
    // when
	underTest = new SimpleCipherTool("abc");
    String actual = underTest.cipher("aaa");
    // then
    assertNotNull(actual);
    assertEquals("bcd", actual);
  }

  @Test
  public void testDecipher() {
    // given
    // when
	underTest = new SimpleCipherTool("arvoia");
    String actual = underTest.decipher("ilpeb://xxccvugtc.pjc/jswgepcfdzptbut/opvqmwppblsh");
    // then
    assertNotNull(actual);
    assertEquals("https://bitbucket.org/arvoiatechtests/sampletaskrp", actual);
  }
}
